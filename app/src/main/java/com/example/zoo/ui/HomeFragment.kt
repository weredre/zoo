package com.example.zooapplication.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoo.R
import com.example.zoo.databinding.FragmentHomeBinding

class HomeFragment: Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also { _binding = it }.root

    // Utilize HomeFragmentDirections from SafeArgs to access fragments
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Elephant
        binding.btnElephant.setOnClickListener {
            // SafeArgs method
            val elephant = HomeFragmentDirections.actionHomeFragmentToDetailsFragments(getString(R.string.elephant_name), getString(R.string.elephant_fact))
            findNavController().navigate(elephant)
        }
        // Giraffe
        binding.btnGiraffe.setOnClickListener {
            val giraffe = HomeFragmentDirections.actionHomeFragmentToDetailsFragments(getString(R.string.giraffe_name), getString(R.string.gorilla_fact))
            findNavController().navigate(giraffe)
        }

        // Tiger
        binding.btnTiger.setOnClickListener {
            val tiger = HomeFragmentDirections.actionHomeFragmentToDetailsFragments(getString(R.string.tiger_name), getString(R.string.tiger_fact))
            findNavController().navigate(tiger)
        }

        // Gorilla
        binding.btnGorilla.setOnClickListener {
            val gorilla = HomeFragmentDirections.actionHomeFragmentToDetailsFragments(getString(R.string.gorilla_name), getString(R.string.gorilla_fact))
            findNavController().navigate(gorilla)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}