package com.example.zooapplication.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapplication.R
import com.example.zooapplication.databinding.FragmentHelloBinding

class HelloFragment : Fragment() {

    private var _binding: FragmentHelloBinding? = null
    private val binding get() = _binding!!

    // Created: CreateView -> ViewCreated -> ViewStateRestored
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHelloBinding.inflate(inflater, container, false).also { fragmentHelloBinding ->
        _binding = fragmentHelloBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // SafeArgs Method -> pass variable as an argument in navigate()
        val nextBtn = HelloFragmentDirections.actionHelloFragmentToGettingStartedFragment()
        binding.btnNext.setOnClickListener {
            findNavController().navigate(nextBtn)
        }
    }

    // Destroyed
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}