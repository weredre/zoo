package com.example.zoo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment

class ZooActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo)

        // intent method from Activity class, .getStringExtra method from Intent class
        val zooNm : String = intent.getStringExtra("zooKey").toString()
        Toast.makeText(this, "Welcome to the $zooNm", Toast.LENGTH_SHORT).show()
//        NavHostFragment.create(R.navigation.zoo_nav_graph)
    }
}